#include <iostream>
#include <sstream>
#include <cassert>
#include <alloca.h>
#include "ObjectPool.hxx"

using namespace std;
using namespace ribomation::memory;

struct Node {
    unsigned      argument;
    unsigned long result;
    Node* next;

    Node(unsigned int argument, unsigned long result, Node* next)
            : argument(argument), result(result), next(next) {}

    string toString() const {
        ostringstream buf;
        buf << "f(" << argument << ") = " << result << " @ " << reinterpret_cast<const unsigned long>(this);
        return buf.str();
    }

    friend ostream& operator <<(ostream& os, const Node& n) {
        return os << n.toString();
    }
};

unsigned long fibonacci(unsigned n) {
    if (n <= 2) return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}


void print(Node* node) {
    if (node == nullptr) return;
    print(node->next);
    cout << *node << endl;
}

void dispose(Node* node, Pool<Node, 100>& m) {
    if (node == nullptr) return;
    dispose(node->next, m);
    node->~Node();
    m.dispose(node);
}

void base(unsigned n) {
    cout << "--- ENTER n=" << n << " ---\n";

    //Pool<Node, 100> m;
    Pool<Node, 100>* mp = new (alloca(sizeof(Pool<Node, 100>))) Pool<Node, 100>{};
    Pool<Node, 100>& m = *mp;

    Node* first = nullptr;
    for (auto arg = 1U; arg <= n; ++arg) {
        first = new (m.alloc()) Node{arg, fibonacci(arg), first};
    }

    cout << "--- PRINT ---\n";
    print(first);
    cout << "--- DISPOSE ---\n";
    dispose(first, m);
    cout << "--- EXIT ---\n";
    assert(m.free() == 100);
}


int main() {
    cout << "[main] enter" << endl;

    base(10);
    base(42);
    base(20);

    cout << "[main] exit" << endl;
    return 0;
}
