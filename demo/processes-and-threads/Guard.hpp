#pragma once
#include "Mutex.hpp"

class Guard {
    Mutex& mutex;

public:
    Guard(Mutex& mutex) : mutex(mutex) {
        mutex.lock();
    }

    ~Guard() {
        mutex.unlock();
    }

    Guard() = delete;
};


