#include <stdlib.h>
#include <stdio.h>
#include <alloca.h>

struct sNode {
    int             arg, value;
    struct sNode*   next;
};
typedef struct sNode Node;

int fibonacci(int n) {
    return n <= 2 ? 1 : fibonacci(n-1) + fibonacci(n-2);
}

void print(Node* n) {
    if (n == 0) return;
    printf("** fib(%d) = %d\n", n->arg, n->value);
    print(n->next);
}

void destroy(Node* n) {
	if (n == 0) return;
	destroy(n->next);
	free(n);
}

void base(int arg) {
    Node*   first = 0;
    for (; arg > 0; --arg) {
        Node* n = alloca( sizeof(Node) );
        //Node* n = malloc( sizeof(Node) );
        n->arg   = arg;
        n->value = fibonacci(arg);
        n->next  = first;
        first = n;
    }
    print(first);
	//destroy(first);
}

int main(int numArgs, char* args[])  {
    base( numArgs == 1 ? 5 : atoi(args[1]) );
    return 0;
}

