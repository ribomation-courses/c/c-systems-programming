#!/bin/bash
set -e
set -x

CFLAGS="-std=c99 -fmax-errors=1 -Wall -Wextra"

gcc $CFLAGS alloc-spy.c -o alloc-spy
gcc $CFLAGS heap-overflow.c -o heap-overflow
gcc $CFLAGS heap-spy.c -o heap-spy
gcc $CFLAGS stack-mem.c -o stack-mem


