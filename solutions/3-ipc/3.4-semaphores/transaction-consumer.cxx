

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdexcept>
#include <unistd.h>
#include "FileSemaphore.hxx"

using namespace std;
using namespace ribomation::process;


int main() {
    string    filename = "./transactions.txt";
    string    semaname = "transactions";
    Semaphore notEmpty{semaname, 0, true};

    cout << "[cons] started" << endl;
    long lastPos = 0;
    bool running = true;
    do {
        notEmpty.wait();

        ifstream transFile{filename};
        if (!transFile) {
            throw runtime_error("cannot open " + filename);
        }

        transFile.seekg(lastPos, ios::beg);
        string line;
        getline(transFile, line);
        lastPos = transFile.tellg();

        cout << "[cons] recv: " << line << endl;
        running = (line != "QUIT");
    } while (running);

    return 0;
}
