#pragma once

#include <string>
#include <stdexcept>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <cerrno>
#include <cstring>

namespace ribomation {
    namespace process {
        using namespace std;

        class Semaphore {
            const string name;
            sem_t* sema;
            bool owner;

        public:
            Semaphore(const string& name, int count, bool openIfExists = false) : name{
                    name[0] == '/' ? name : '/' + name} {
                sema  = sem_open(name.c_str(), O_CREAT | (openIfExists ? O_EXCL : 0), S_IRUSR | S_IWUSR, count);
                if (sema == SEM_FAILED) {
                    if (errno == EEXIST && openIfExists) {
                        sema = sem_open(name.c_str(), 0);
                    } else {
                        throw runtime_error(string("failed to create FS semapahore ") + name + ": " + strerror(errno));
                    }
                }
                owner = true;
            }

            Semaphore(const string& name) {
                sema = sem_open(name.c_str(), 0);
                if (sema == SEM_FAILED)
                    throw runtime_error(string("failed to open FS semapahore ") + name + ": " + strerror(errno));
                owner = false;
            }

            ~Semaphore() {
                sem_close(sema);
                if (owner)
                    sem_unlink(name.c_str());
            }

            Semaphore& notOwner() {
                owner = false;
                return *this;
            }

            void wait() { sem_wait(sema); }

            void signal() { sem_post(sema); }

            void signal(unsigned n) {
                while (n-- > 0) signal();
            }
        };
    }
}
