//COMPILE: c99 -g -Wall -D_BSD_SOURCE fibonacci.c -o fibonacci -lrt -lpthread
//RUN    : ./fibonacci [-n <max-arg>] [-q] [-v]

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>


int debug = 0;

void  error(const char* msg) {
    fprintf(stderr, "error: %s\n", msg); 
    perror("fibonacci");
    exit(1);
}

// -----------------------------------------------------
// --- Shared Memory ---
// -----------------------------------------------------

// Creates a new SHM segment
void*  shm_create(size_t numBytes) {
    int  pageSz = getpagesize();
    int  shmSz  = (1 + (numBytes / pageSz)) * pageSz;

    int shmFD = shm_open("/fibonacci.shm", O_CREAT | O_TRUNC | O_RDWR, 0666);
    if (shmFD < 0) error("Cannot create SHM");
    if (ftruncate(shmFD, shmSz) < 0) error("Cannot re-size SHM");

    void* shm = mmap(0, shmSz, PROT_READ | PROT_WRITE, MAP_SHARED, shmFD, 0);
    if(shm == MAP_FAILED) error("Cannot map SHM");
    close(shmFD);

    return shm;
}

// SHM simple malloc()
void*  shm_alloc(size_t numBytes, void** startAddress) {
    void*  address = *startAddress;
    *startAddress += numBytes;
    return address;
}

// -----------------------------------------------------
// --- Channel ---
// -----------------------------------------------------
typedef struct {
    sem_t    notFull;
    sem_t    notEmpty;
    int*     queue;
    int      capacity, putIdx, getIdx;
} Channel;
#define SHARED 1

// Constructor
Channel*  chan_create(int capacity, void** addr) {
    Channel*  chan = (Channel*)shm_alloc(sizeof(Channel), addr);
    
    chan->queue    = (int*)shm_alloc(capacity * sizeof(int), addr);
    chan->capacity = capacity;
    chan->putIdx   = 0;
    chan->getIdx   = 0;
    sem_init(&(chan->notFull) , SHARED, capacity);
    sem_init(&(chan->notEmpty), SHARED, 0);
    
    return chan;
}

// Inserts a message and blocks if full
void chan_put(Channel* chan, int x) {
    sem_wait( &(chan->notFull) );
    
    chan->queue[ chan->putIdx++ ] = x;
    if (chan->putIdx >= chan->capacity) chan->putIdx = 0;
    
    sem_post( &(chan->notEmpty) );
}

// Removes the first message and blocks if empty
int  chan_get(Channel* chan) {
    sem_wait( &(chan->notEmpty) );
    
    int x = chan->queue[ chan->getIdx++ ];
    if (chan->getIdx >= chan->capacity) chan->getIdx = 0;
    
    sem_post( &(chan->notFull) );
    return x;
}


// -----------------------------------------------------
// --- Tasks ---
// -----------------------------------------------------

// --- The Client ---
void fibonacciClient(int N, Channel* send, Channel* recv) {
    if (debug) printf("[client] started, N=%d\n", N);

    for (int n = 1; n <= N; ++n) {
        if (debug) printf("[client] put(%d)\n", n);
        chan_put(send, n);
        int result = chan_get(recv);
        printf("[client] fib(%d) = %d\n", n, result);
    }
    chan_put(send, -1);

    if (debug) printf("[client] done\n");
}

// --- The Server ---
int  fib(int n) {return n <= 2 ? 1 : fib(n-1) + fib(n-2);}

void fibonacciServer(Channel* in, Channel* out) {
    if (debug) printf("[server] started\n");

    int n = -1;
    while ( (n = chan_get(in)) > 0) {
        if (debug) printf("[server] get() -> %d\n", n);
        int result = fib(n);
        chan_put(out, result);
        if (debug) printf("[server] put(%d)\n", result);
    }
    
    if (debug) printf("[server] done\n");
}

// -----------------------------------------------------
// --- Main Entry ---
// -----------------------------------------------------
#define EQ(s1,s2)   (strcmp(s1,s2)==0)
int main(int numArgs, char* args[]) {
    int N = 42;

    for (int k=1; k<numArgs; ++k) {
        const char* arg = args[k];
        if (EQ(arg, "-n")) {
            N = atoi( args[++k] );
        } else if (EQ(arg, "-v")) {
            debug = 1;
        } else if (EQ(arg, "-q")) {
            debug = 0;
        } else {
            fprintf(stderr, "usage: %s [-n <max-arg>] [-q] [-v]\n", args[0]);
            exit(1);
        }
    }
    
    const int   qSize   = 1;
    size_t      shmSize = 2 * (sizeof(Channel) + qSize * sizeof(int));
    void*       shm     = shm_create(shmSize);

    if (debug) {
        printf("[main] Num Args = %d\n", N);
        printf("[main] SHM Size = %u bytes\n", (unsigned)shmSize);
        printf("[main] SHM Addr = %p\n", shm);
    }

    void*       addr  = shm;    
    Channel*    sendQ = chan_create(qSize, &addr);
    Channel*    recvQ = chan_create(qSize, &addr);
    if (fork() == 0) {
        fibonacciClient(N, sendQ, recvQ);
        exit(0);
    } else {
        fibonacciServer(sendQ, recvQ);
    }
    wait(NULL);

    return 0;
}

