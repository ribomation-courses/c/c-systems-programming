//COMPILE: c99 -g -Wall -D_POSIX_SOURCE fibonacci.c -o fibonacci
//RUN    : ./fibonacci <arg to fib> <timeout secs>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <setjmp.h>
#include <string.h>
#include <time.h>

void  error(char* msg) {
  printf("error: %s\n", msg); 
  perror("fibonacci");
  exit(1);
}

#define TIMEOUT  1
jmp_buf   executionTimeExceeded;

void timeout(int signo) {
  printf("Got timeout\n");
  longjmp(executionTimeExceeded, TIMEOUT);
}

void setup() {
  sigset_t  mask;

  sigemptyset(&mask);
  sigaddset(&mask, SIGALRM);
  if (sigprocmask(SIG_UNBLOCK, &mask, NULL) != 0)
    error("Failed to set SIG MASK");
}

void registerHandler() {
  struct sigaction  action;

  memset(&action, 0, sizeof(action));
  action.sa_handler = timeout;
  if (sigaction(SIGALRM, &action, NULL) != 0)
    error("Failed to register signal handler");
}

long compute(long (*f)(int), int arg, int maxSecs) {    
    registerHandler();

    long result = 0;
    int rc;
    if ((rc = setjmp(executionTimeExceeded)) == 0) {
        alarm((unsigned) maxSecs);
        result = (*f)(arg);
    } else if (rc < 0) {
        error("Cannot setjmp");
    } else if (rc == TIMEOUT) {
        printf("Max execution time of %d sec(s) exceeded\n", maxSecs);
    } else {
        error("Unknown ERROR");
    }

    return result;
}


long fibonacci(int n) {
  return n <= 2 ? 1 : fibonacci(n-1) + fibonacci(n-2);
}


int main(int argc, char* argv[]) {
  int  n = 44, t = 5;
  if (argc > 1) n = atoi( argv[1] );
  if (argc > 2) t = atoi( argv[2] );
  printf("Computing fibonacci(%d), with timeout %d sec(s)\n", n, t);

  setup();
  clock_t  start  = clock();
  long     result = compute(fibonacci, n, t);
  clock_t  stop   = clock();
  
  printf("fibonacci(%d) = %ld (elapsed %.3f secs)\n", 
         n, result, (double)(stop - start) / CLOCKS_PER_SEC);

  return  0;
}

