//COMPILE: c99 -g -Wall -D_POSIX_SOURCE clock.c -o clock
//RUN    : ./clock


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>

#define MICROSECS    (1000 * 1000)
typedef void (*sighandler_t)(int);

void  error(char* msg) {
  printf("error: %s\n", msg); 
  perror("timer_test");
  exit(1);
}

void timeout(int signo) {
  time_t  currentTime = time(NULL);
  printf("Time: %s", ctime(&currentTime));
}

void setupSignal(int what, int sig) {
  sigset_t  mask;

  sigemptyset(&mask);
  sigaddset(&mask, sig);
  if (sigprocmask(what, &mask, NULL) != 0)
    error("Failed to set SIG MASK");
}

void registerHandler(int sig, sighandler_t hdlr) {
  struct sigaction  action;
  memset(&action, 0, sizeof(action));
  action.sa_handler = hdlr;
  if (sigaction(sig, &action, NULL) != 0)
    error("Failed to register SIG handler");
}

void initTimer(struct itimerval* t, float delay, float interval) {
  t->it_value.tv_sec     = (int)delay;
  t->it_value.tv_usec    = (int)( (delay - t->it_value.tv_sec) * MICROSECS );
  t->it_interval.tv_sec  = (int)interval;
  t->it_interval.tv_usec = (int)( (interval - t->it_interval.tv_sec) * MICROSECS );
}

void startTimer(float initialDelay, float interval, sighandler_t hdlr) {
  struct itimerval  timer;
  initTimer(&timer, initialDelay, interval);

  if (setitimer(ITIMER_REAL, &timer, NULL) != 0) 
	error("Cannot register interval timer");
  
  setupSignal(SIG_UNBLOCK, SIGALRM);
  registerHandler(SIGALRM, hdlr);
}

int main() {
  startTimer(0.5, 1, timeout);
  for (;;) pause();
}

