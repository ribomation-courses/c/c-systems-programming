// compile: gcc -c fibsrv.o
// link   : gcc intq.o fibsrv.o -o fibsrv -lrt

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "intq.h"

long fibonacci(int n) {
  return n <= 2 ? 1 : fibonacci(n-1) + fibonacci(n-2);
}

static char*	reqQname  = "/fibRequest";
static char*	rplyQname = "/fibReply";
static mqd_t	reqQ;
static mqd_t	rplyQ;

void  cleanup() {
	printf("[fibsrv] cleanup\n");
	mqClose(reqQ);
	mqClose(rplyQ);
	mqDispose(reqQname);
	mqDispose(rplyQname);
}

void shutdown(int signo) {
	cleanup();
	exit(0);
}

int main() {
	reqQ  = mqCreate(reqQname, 10, MSGSZ);
	rplyQ = mqCreate(rplyQname, 10, MSGSZ);
	atexit(cleanup);
	signal(SIGINT, shutdown);

	do {
		unsigned prio;
		Message msg;
		printf("[fibsrv] waiting...\n");
		mqReceive(reqQ, &msg, &prio);
		
		int  n = (int)msg;
		printf("[fibsrv] RECV n=%d\n", n);
		if (n <= 0) break;
		
		long result = fibonacci(n);
		printf("[fibsrv] RPLY fib(%d) = %ld\n", n, result);
		
		msg = result;
		mqSend(rplyQ, &msg, 1);
	} while (1);

	return 0;
}
