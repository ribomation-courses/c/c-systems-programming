// compile: gcc -c fibcli.o
// link: cc -std=c99 intq.o fibcli.o -o fibcli -lrt

#include <stdio.h>
#include <stdlib.h>
#include "intq.h"

static char*	reqQname  = "/fibRequest";
static char*	rplyQname = "/fibReply";

char* defaultArgs[] = {"fibcli", "10", "20", "30", "35", "40", "42", "45", "-1", NULL};

int main(int numArgs, char* args[]) {
	if(numArgs == 1) {
		args    = defaultArgs;
		numArgs = ( sizeof(defaultArgs) / sizeof(char*) ) - 1;
	}

	mqd_t  reqQ  = mqOpen(reqQname);
	mqd_t  rplyQ = mqOpen(rplyQname);
	int idx=1;
	for (; idx<numArgs; ++idx) {
		Message  arg = atoi( args[idx] );
		printf("[fibcli] sending n=%ld\n", (long)arg);
		mqSend(reqQ, &arg, 1);
		if (arg <= 0) break;
		
		Message result = 0;
		mqReceive(rplyQ, &result, NULL);
		printf("[fibcli] fib(%ld) = %ld\n", (long)arg, (long)result);
	}
	
	mqClose(reqQ);
	mqClose(rplyQ);

	return 0;
}
