#pragma  once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <mqueue.h>

typedef long Message;
#define MSGSZ  sizeof(Message)

extern mqd_t mqCreate(char* name, int maxMsgCnt, int maxMsgSz);

extern mqd_t mqOpen(char* name);

extern void mqClose(mqd_t q);

extern void mqDispose(char* name);

extern void mqSend(mqd_t q, Message* msg, int prio);

extern Message* mqReceive(mqd_t q, Message* msg, unsigned* prio);

extern unsigned mqSize(mqd_t q);


