#!/bin/bash
set -ex

CC="c99 -g -Wall"
LIBS=-lrt

/bin/rm -rf *.o fibsrv fibcli

$CC -c intq.c
$CC -c fibsrv.c
$CC -c fibcli.c
$CC intq.o fibsrv.o -o fibsrv $LIBS
$CC intq.o fibcli.o -o fibcli $LIBS

