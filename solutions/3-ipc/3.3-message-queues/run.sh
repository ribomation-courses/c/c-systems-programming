#!/usr/bin/env bash
set -e
set -x

mkdir -p build
cd build
cmake -G 'Unix Makefiles' ..
make

./fibsrv &

sleep 2
ls -lhF /dev/mqueue

./fibcli
