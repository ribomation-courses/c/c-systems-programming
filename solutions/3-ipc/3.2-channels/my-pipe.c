//COMPILE: c99 -g -Wall my-pipe.c -o my-pipe
//RUN    : ./my-pipe <producer> <consumer>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void child(const char* exe, int fdToUse, int fdToClose, int fdToRedirect) {
    pid_t  pid = fork();
    if (pid < 0) {perror("my-pipe"); exit(1);}
    if (pid > 0) {
        close(fdToUse);
        return;
    }

    fprintf(stderr, "child(%s, %d, %d, %d, %d)\n",
            exe, fdToUse, fdToClose, fdToRedirect, getpid()
    );
    close(fdToClose);
    dup2(fdToUse, fdToRedirect);
    
    execl(exe, exe, NULL);
    fprintf(stderr, "EXEC failed\n");
    exit(42); //not reachable
}

void mkPipe(const char* producer, const char* consumer) {
    int  fd[2];
    if (pipe(fd) < 0) {perror("my-pipe"); exit(1);}
    
    child(producer, fd[1], fd[0], STDOUT_FILENO);
    child(consumer, fd[0], fd[1], STDIN_FILENO);
}

int main(int numArgs, char* args[]) {
    char* producer = "/bin/ls";
    char* consumer = "/usr/bin/wc";

    if (numArgs >= 2) {
        producer = args[1];
    }
    if (numArgs >= 3) {
        consumer = args[2];
    }

    mkPipe(producer, consumer);
    wait(NULL); wait(NULL);
    
    return 0;
}

