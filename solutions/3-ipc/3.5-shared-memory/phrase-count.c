//COMPILE: c99 -g -Wall -D_BSD_SOURCE -D_GNU_SOURCE phrase-count.c -o phrase-count -lrt
//RUN    : ./phrase-count [-p <phrase>] [-w <workers>] [-f <file>]

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>

extern char* strcasestr(const char*, const char*);

void error(char* msg) {
    printf("error: %s\n", msg);
    perror("phrase-count");
    exit(1);
}

size_t fileSize(const char* filename) {
    struct stat fileInfo;
    if (stat(filename, &fileInfo) < 0) error("cannot stat file");
    return (size_t) fileInfo.st_size;
}

char* loadFile(const char* filename, const size_t fileSz) {
    int fd = open(filename, O_RDONLY);
    if (fd < 0) error("Cannot open file");

    char* contents = mmap(0, fileSz, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (contents == MAP_FAILED) error("Failed to map file");
    close(fd);  //no need for the FD any longer
    contents[fileSz] = '\0'; //end-of-string

    printf("Mapped file %s to addr %p with size %ld bytes\n",
           filename, (void*) contents, (long int) fileSz);
    return contents;
}

int doCount(const char* phrase, char* begin, const char* end) {
    int count = 0, phraseSz = (int) strlen(phrase);
    while (begin < end) {
        char* p = strcasestr(begin, phrase);
        if (!p) break;
        count++;
        begin = p + phraseSz;
    }
    return count;
}

void* createShm(const char* shmName, int size) {
    int pageSz = getpagesize();
    int shmSz = (1 + (size / pageSz)) * pageSz;

    int shmFD = shm_open(shmName, O_CREAT | O_TRUNC | O_RDWR, 0666);
    if (shmFD < 0) error("Cannot create SHM");
    if (ftruncate(shmFD, shmSz) < 0) error("Cannot re-size SHM");

    void* shm = mmap(0, (size_t) shmSz, PROT_READ | PROT_WRITE, MAP_SHARED, shmFD, 0);
    if (shm == MAP_FAILED) error("Cannot map SHM");
    close(shmFD);
    printf("SHM created, %d bytes\n", shmSz);

    return shm;
}

int createProcess() {
    pid_t pid = fork();
    if (pid < 0) error("Failed to fork()");
    return (pid == 0);
}

#define EQ(s1, s2)   (strcmp(s1,s2)==0)

int main(int numArgs, char* args[]) {
    char* phrase     = "Hamlet";
    char* fileName   = "./shakespeare.txt";
    char* shmName    = "/shakespeare.shm";
    int   numWorkers = 16;

    for (int k = 1; k < numArgs; ++k) {
        char* arg = args[k];
        if (EQ(arg, "-w")) {
            numWorkers = atoi(args[++k]);
        } else if (EQ(arg, "-p")) {
            phrase = args[++k];
        } else if (EQ(arg, "-f")) {
            fileName = args[++k];
        } else {
            fprintf(stderr, "usage: %s [-p <phrase>] [-w <workers>] [-f <file>]", args[0]);
            exit(1);
        }
    }


    size_t  textSize    = fileSize(fileName) + 1;
    char*   fileContent = loadFile(fileName, textSize);
    size_t  shmSize     = textSize + numWorkers * sizeof(int);
    char*   shm         = createShm(shmName, (int) shmSize);
    int*    results     = (int*) (shm + textSize);
    int     chunkSize   = (int) (textSize / numWorkers);

    memcpy(shm, fileContent, textSize); //move the src into place

    printf("Launching %d workers, each with chunk-size=%d\n", numWorkers, chunkSize);
    for (int id = 0; id < numWorkers; ++id) {
        if (createProcess()) {
            char*  begin = &shm[id * chunkSize] + (id == 0 ? 0 : 1);
            char*  end   = &shm[(id + 1) * chunkSize];
            *end = '\0';

            int count = doCount(phrase, begin, end);
            results[id] = count;

            printf("[Worker-%d] Phrase '%s' occurs %d times. Text segment = [%ld, %ld]\n",
                   id + 1, phrase, count, (begin - shm), (end - shm));
            exit(count);
        }
    }

    for (int id = 1; id <= numWorkers; ++id) { wait(NULL); }

    int result = 0;
    for (int id = 0; id < numWorkers; ++id) { result += results[id]; }
    printf("The phrase '%s' was found %d time(s)\n", phrase, result);

    shm_unlink(shmName);
    return 0;
}
