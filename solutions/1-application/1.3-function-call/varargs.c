//COMPILE: c99 -Wall -g varargs.c -o varargs
//RUN    : ./varargs

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

double average(int num, ...) {
    printf("[average] enter: 1st arg=%d\n", num);
    int sum = num;
    int cnt = 1;

    va_list args;
    va_start(args, num);
    int n;
    while ((n = va_arg(args, int)) != 0) {
        sum += n;
        cnt++;
    }
    va_end(args);

    double result = (double) sum / cnt;
    printf("[average] exit: result=%g\n", result);
    return result;
}

int main() {
    printf("[main] enter\n");
    double m = average(2, 2, 4, 4, 6, 6, 8, 8, 0);
    printf("[main] avg=%g\n", m);

    m = average(1, -2, -3, 3, -2, 1, 0);
    printf("[main] avg=%g\n", m);

    m = average(1, 0);
    printf("[main] avg=%.4f\n", m);

    return 0;
}

