cmake_minimum_required(VERSION 3.5)
project(1_1_address_space)


set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Werror -Wfatal-errors")
add_executable(accounts accounts.c)

