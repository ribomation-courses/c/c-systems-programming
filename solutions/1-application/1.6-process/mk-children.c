//COMPILE: c99 -g -Wall mk-children.c -o mk-children
//RUN    : ./mk-children <num-childs>

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void child(int id) {
    printf("[child-%d] pid=%d, parent=%d\n", id, getpid(), getppid());
    sleep(1);
    exit(id);
}

int main(int argc, char* argv[]) {
    int numChilds = 1000;
    if (argc > 1) numChilds = atoi(argv[1]);

    printf("[parent] creating %d child processes\n", numChilds);
    for (int id = 1; id <= numChilds; ++id) {
        pid_t pid = fork();
        if (pid < 0) {
            perror("fork");
            exit(1);
        }
        if (pid == 0) child(id);
    }

    while (numChilds-- > 0) {
        int status;
        pid_t pid = wait(&status);
        if (pid < 0) {
            perror("parent");
            return 1;
        }
        printf("[parent] child=%d, terminated, code=%d\n",
               pid, WEXITSTATUS(status));
    }

    return 0;
}
