cmake_minimum_required(VERSION 3.8)
project(09_basic_io)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Wpedantic -Werror -Wfatal-errors -Wno-sign-compare")

add_executable(logger logger.c)
