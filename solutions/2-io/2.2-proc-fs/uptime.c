#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int numCPUs() {
    FILE* f =fopen("/proc/cpuinfo", "r");
    if (f==NULL) {
        perror("numCPUs");
        exit(1);
    }

    int cpuCount = 0;
    char line[128];
    while (fgets(line, 128, f)) {
        if (strstr(line, "processor") != NULL) cpuCount++;
    }

    return cpuCount;
}

char* toTimeString(int time)  {
    const int minute = 60;
    const int hour   = 60 * minute;
    const int day    = 24 * hour;

    static char buf[128];
    sprintf(buf, "%02d:%02d:%02d",
            (time % day) / hour,
            (time % hour) / minute,
            time % minute
    );
    return buf;
}

int main() {
    FILE* f = fopen("/proc/uptime", "r");
    if (f == NULL) {
        perror("uptime");
        return 1;
    }

    double boot, idle;
    fscanf(f, "%lf %lf", &boot, &idle);

    printf("boot=%.0f, idle=%.0f, cpus=%d\n", boot, idle, numCPUs());
    printf("boot=%s, idle=%s\n", toTimeString((int) boot), toTimeString((int) (idle / numCPUs())));

    return 0;
}

